"""utils function"""
import heapq as heap
from Population import Population
import constants
import random
import ponyge
from Individual import Individual


def to_minimum_heap(list):
    """returns the best individual of a list"""
    h = []
    for i in range(len(list)):
        heap.heappush(h, (list[i].get_fitness(), list[i]))
    return heap.heappop(h)


def mutate(population):
    """mutates the given population"""

    mutated = Population()
    for i in population.get_members():
        i.mutate()
        mutated.add_member(i)

    return mutated


def survivor_selection(previuosgeneration, best):
    """returns the new generation to use"""
    new_generation = Population()

    new_generation.add_member(best)
    worst = previuosgeneration.get_worst()
    for i in range(previuosgeneration.get_size()):
        individual = previuosgeneration.get_members()[i]

        if individual is not worst:
            new_generation.add_member(previuosgeneration.get_members()[i])

    return new_generation


def local_search(individual):
    """applies a local search for the given individual"""
    neighbours = generate_neighbours(individual)
    best = individual
    for i in neighbours:
        if i.get_fitness() < individual.get_fitness():

            individual.set_codons(i.get_codons())


def generate_neighbours(individual):
    """returns a list with generated neighbours"""
    neighbours = []
    neigbour = Individual()
    while len(neighbours) < constants.neighbours:
        neigbour.set_codons(individual.get_representation()[:])
        for i in range(5):
            neigbour.set_codon(random.randint(0, 99), random.randint(0,len(neigbour.get_representation())-1))
        evaluate = ponyge.eval_or_exec(individual.get_mapping().replace("X", "1"))
        if evaluate is not False:
                neighbours.append(neigbour)
        # mapping = neigbour.get_mapping()
        # mapping2 = individual.get_mapping()
    return neighbours



