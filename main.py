"""genetic programming algorithm main class"""
from Population import Population
import selection
import recombination
import plot
import utils
import mapping
import ponyge
import copy

population = Population()
population.populate(50)


#
# parents = selection.parent_selection(population)
# sons = recombination.recombine(parents)
# meu = sons.get_members()[:]
# # plot.plot_result(population.get_best()[1].get_mapping())
# # plot.plot_result(sons.get_best()[1].get_mapping())
# mutated = utils.mutate(sons)
# mutated.print_population()
# print(str(mutated.get_worst().get_fitness()) + "hi")
# new_generation= utils.survivor_selection(mutated)
# print(str(new_generation.get_worst().get_fitness()))
# # new_generation.print_population()

def evolution(iterations, start):
    for i in range(iterations):
        best = copy.deepcopy(start.get_best()[1])
        if start.get_best()[0] > 0.1:
            parents = selection.parent_selection(start)

            sons = recombination.recombine(parents)
            mutated = utils.mutate(sons)
            new_generation = utils.survivor_selection(mutated, best)
            #local search
            # best_percentage=new_generation.get_best_percentage()
            # for one_of_best in best_percentage.get_members():
            #     utils.local_search(one_of_best)
            print(i)
            print(str(new_generation.get_best()[1].get_mapping()))
            print(new_generation.get_best()[0])
            start = new_generation
        else:
            plot.plot_result(new_generation.get_best()[1].get_mapping())

            print(new_generation.get_best()[0])
            return new_generation.get_best()[0]

    plot.plot_result(new_generation.get_best()[1].get_mapping())

    return new_generation.get_best()[0]


evolution(500, population)
