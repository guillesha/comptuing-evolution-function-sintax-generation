"""defines the process for over_selection in the parent selection tournament selection"""
import random
import constants
import utils
from Population import Population


def parent_selection(population):
    parents = Population()
    """function which selects the parents
    @:param population
    """
    """
    Created on Thu Nov 16 13:24:11 2017

    @author: guille
    """
    while parents.get_size() < population.get_size():
        current_selection = []
        for i in range(constants.k):
            rand = random.randint(0, population.get_size() - 1)
            current_selection.append(population.get_members()[rand])
        best_individual = utils.to_minimum_heap(current_selection)[1]
        parents.add_member(best_individual)
    return parents
