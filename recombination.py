"""defines the procedures for the recombination"""
from Population import Population
import random
import constants
from Individual import Individual
import ponyge
import copy

def recombine(parents):
    """recombine the list of parents into new child
    @param parents"""
    sons = Population()

    while sons.get_size() < parents.get_size():
        i= sons.get_size()
        father1 = copy.deepcopy(parents.get_members()[random.randint(0, parents.get_size() - 1)])
        father2 = copy.deepcopy(parents.get_members()[random.randint(0, parents.get_size() - 1)])
        father_1_rep = father1.get_codons()
        father_2_rep = father2.get_codons()
        if random.uniform(0, 1) < constants.recombination_probability:
            son1 = Individual()
            son2 = Individual()
            rand_rec_point = random.randint(0, len(father_1_rep))
            if len(father_1_rep) > len(father_2_rep):
                shorter = father_2_rep
            else:
                shorter = father_1_rep
            for i in range(len(shorter)):
                if i < rand_rec_point:
                    son1.add_codon(father_1_rep[i])
                    son2.add_codon(father_2_rep[i])
                else:
                    son1.add_codon(father_2_rep[i])
                    son2.add_codon(father_1_rep[i])
            # we add the son if it is valid
            evaluate_1_son = ponyge.eval_or_exec(son1.get_mapping().replace("X", "1"))
            evaluate_2_son = ponyge.eval_or_exec(son1.get_mapping().replace("X", "1"))
            if evaluate_1_son is not False:
                sons.add_member(son1)
            else:
                sons.add_member(father1)
            if evaluate_2_son is not False:
                sons.add_member(son2)
            else:
                sons.add_member(father2)
        else:
            sons.add_member(father1)
            sons.add_member(father2)

    return sons
