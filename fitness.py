"""calculate the fitness of each individual"""

from mapping import map_to_function
from ponyge import eval_or_exec
import copy
import math
from constants import *


def fitness(individual):
    """ Returns the fitness of a given individual"""
    # we map the current individual
    mapped_function = map_to_function(individual)
    # value for current individual considering formula 7 on activity
    value_current_individual = 0
    value_current_function = 0
    # print(mapped_function+" mapped function")
    sum_error = 0
    for i in range(N):
        fa1 = (a + i * ((b - a) / N)) + h  # a+i*Ax /h
        fa2 = (a + i * ((b - a) / N))
        value_iteration_individual = (eval_or_exec(replace_string(mapped_function, str(fa1))) - eval_or_exec(
            replace_string(mapped_function, str(fa2)))) / h

        value_iteration_function = eval_or_exec(replace_string(function_to_solve, str(fa2)))
        value_current_individual += value_iteration_individual
        value_current_function += value_iteration_function
        # we define the variable W from the error function
        comparison = abs(value_current_individual - value_current_function)
        if comparison <= U:

            w = K1
        else:
            w = K2
        sum_error += w * comparison
    fitness_individual = (1 / (N + 1)) * sum_error
    if abs(eval_or_exec(mapped_function.replace("X", "0")) - fzero) < 1:
        final_fitness = fitness_individual
    else:
        final_fitness = fitness_individual + 60
    # print(str(fitness_individual)+" fitness")

    return final_fitness


def replace_string(string, value):
    """Replaces the string for the given value
    @:param value"""
    new_string = copy.copy(string)
    return new_string.replace("X", value)
