"""class that represents the each individual"""

from fitness import fitness
from mapping import map_to_function
from constants import *
import ponyge
from random import randint, uniform
import ponyge


class Individual:

    def __init__(self):
        self.representation = []  # must be a list of integers
        self.fitness = -1
        self.mapping = 0

    def get_fitness(self):
        if self.fitness != -1:
            return self.fitness
        if not ponyge.eval_or_exec(self.get_mapping().replace("X", "1")):
            self.fitness = 500
            return self.fitness
        else:

            self.fitness = fitness(self.representation)

            return self.fitness

    def initialize(self):
        """ creates the representation"""
        length = randint(representation_low, representation_high)
        for i in range(length):
            self.representation.append(randint(0, 900))

    def get_representation(self):
        return self.representation

    def get_mapping(self):
        """returns the function form of the individual """
        self.mapping = map_to_function(self.representation)
        return map_to_function(self.representation)

    def __gt__(self, other):
        """returns true if the fitness is smaller"""
        return self.fitness > other.fitness

    def print(self):
        return self.fitness

    def get_codons(self):
        return self.representation

    def add_codon(self, codon):
        """adds a new codon to the representation"""
        self.representation.append(codon)

    def set_codon(self, new_codon, position):
        self.representation[position] = new_codon

    def get_codon(self, postion):
        return self.representation[postion]

    def set_codons(self, codons):
        self.representation = codons[:]

    def new_fitness(self):
        self.fitness = fitness(self.representation)

    def mutate(self):
        """mutates the individual"""
        for i in range(len(self.representation)):
            mutated = Individual()
            mutated.set_codons(self.representation)
            rand = uniform(0, 1)
            if rand < mutation_probability:
                rand = randint(0, 900)
                mutated.set_codon(rand, i)

                evaluate = ponyge.eval_or_exec(mutated.get_mapping().replace("X", "1"))
                if evaluate is not False:
                    self.set_codon(rand, i)
        self.new_fitness()
