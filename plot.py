import matplotlib.pyplot as plt
import numpy as np
import constants
import math

def plot_result(mapped_individual="X"):
    """returns a plot with the best solution and the actual solution"""
    X = np.arange(constants.a, constants.b, 0.2)
    Q = np.arange(constants.a, constants.b, 0.2)
    y = eval(constants.solution.replace("math","np"))
    z = eval(mapped_individual.replace("math", "np"))
    plt.plot(Q, y)
    plt.plot(X, z)
    plt.legend(['solution '+constants.solution, 'best_found '+ mapped_individual], loc='upper left')
    plt.show()

