"""population"""

from Individual import Individual
import heapq as heap
import ponyge
import numpy as np
from operator import attrgetter
import constants


class Population:

    def __init__(self):
        self.members = []  # list of individuals
        self.best = -1
        self.worst = 0

    def populate(self, size):
        """creates an initial population of the given size"""
        while len(self.members) < size:
            individual_to_add = Individual()
            individual_to_add.initialize()
            self.members.append(individual_to_add)

    def get_members(self):
        return self.members

    def get_members_mapping(self):
        return [x.get_mapping() for x in self.members]

    def get_best_percentage(self):
        h = []
        for i in range(len(self.members)):
            heap.heappush(h, (self.members[i].get_fitness(), self.members[i]))

        best = Population()
        size_percentage = int(self.get_size() * constants.percentage)
        while (best.get_size() < size_percentage):
            best.add_member(heap.heappop(h)[1])
        return best

    def get_best(self):
        h = []
        for i in range(len(self.members)):
            heap.heappush(h, (self.members[i].get_fitness(), self.members[i]))
        return heap.heappop(h)

    def get_worst(self):

        return max(self.members, key=attrgetter('fitness'))

    def print_population(self):

        for i in self.members:
            print(i.get_mapping())

    def get_size(self):
        """returns population size"""
        return len(self.members)

    def set_members(self, members):
        """sets the members of the population"""
        self.members += members

    def add_member(self, member):

        self.members.append(member)

    def get_average(self):
        list_fitness = []
        for i in self.members:
            list_fitness.append(i.get_fitness())
        return np.mean(list_fitness)
