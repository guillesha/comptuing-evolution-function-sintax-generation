"""calculate the fitness of each individual"""

from ponyge import Grammar
import constants

def map_to_function( individual):
    """returns a mapped function of the individual"""
    grammar = Grammar("ped.bnf")
    a = grammar.generate(
        individual,
        constants.wrapping)
    return a[0]
